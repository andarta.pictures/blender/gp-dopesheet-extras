# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "dopesheet_extra",
    "author" : "Tom VIGUIER, William CLEMENT",
    "description" : "",
    "blender" : (3, 3, 0),
    "version" : (0, 1, 0),
    "location" : "",
    "warning" : "",
    "category" : "Andarta"
}

import bpy
import rna_keymap_ui

class DOPE_PROPS(bpy.types.PropertyGroup):
    bl_label = "gp dopesheet extra properties"
    bl_idname = "dope.properties"

    new_object = []
    previous_active_layers = []
    draw_handler = []

class DOPE_OT_SWITCH_DRAWING_OBJECT(bpy.types.Operator) :
    bl_label = "switch the currently drawing object "
    bl_idname = 'dope.switch'
    bl_options = {'REGISTER'}   

    def execute(self, context):
        new_object = bpy.context.window_manager.dse_data.new_object[0]
        last_object = context.view_layer.objects.active
        if new_object != last_object and new_object.type == 'GPENCIL' :
            bpy.ops.object.mode_set(mode = 'OBJECT')
            for ob in list(context.view_layer.objects) :
                ob.select_set(False)
            new_object.select_set(True)
            context.view_layer.objects.active = new_object
            bpy.ops.object.mode_set(mode = 'PAINT_GPENCIL')
        return {'FINISHED'}

class GP_DV_AddonPref(bpy.types.AddonPreferences):
    bl_idname = __name__
 
    add_g_pencil_tools : bpy.props.BoolProperty(default = True)
    add_switch_on_layer_click : bpy.props.BoolProperty(default = True)

    def draw(self, context):
        layout = self.layout
        layout.prop( self, 'add_g_pencil_tools', text = 'Add Grease Pencil tools to the Dopesheet' )
        layout.prop( self, 'add_switch_on_layer_click', text = 'Add Switch on Dopesheet Layer click' )
        layout.label(text="KeyMap :")
    
        col = layout.column()
        kc = bpy.context.window_manager.keyconfigs.addon
        for km, kmi in addon_keymaps:
            km = km.active()
            col.context_pointer_set("keymap", km)
            rna_keymap_ui.draw_kmi([], kc, km, kmi, col, 0)

def get_active_layers() :
    result = []
    for ob in [o for o in bpy.data.objects if o.type == 'GPENCIL'] :
        if ob.data.layers.active :
           result.append([ob, ob.data.layers.active])
    return result

def draw(self, context):
    layout = self.layout
    st = context.space_data
    tool_settings = context.tool_settings

    # Load preferences
    preferences = context.preferences
    addon_prefs = preferences.addons[__name__].preferences

    # Layer management
    if addon_prefs.add_g_pencil_tools and st.mode == 'DOPESHEET':
        ob = context.active_object
        #selected = st.dopesheet.show_only_selected
        enable_but = ob is not None and ob.type == 'GPENCIL'
        row = layout.row(align=True)
        row.enabled = enable_but
        row.operator("gpencil.layer_add", icon='ADD', text="")
        row.operator("gpencil.layer_remove", icon='REMOVE', text="")
        row.menu("GPENCIL_MT_layer_context_menu", icon='DOWNARROW_HLT', text="")
        row = layout.row(align=True)
        row.enabled = enable_but
        row.operator("gpencil.layer_move", icon='TRIA_UP', text="").type = 'UP'
        row.operator("gpencil.layer_move", icon='TRIA_DOWN', text="").type = 'DOWN'
        row = layout.row(align=True)
        row.enabled = enable_but
        row.operator("gpencil.layer_isolate", icon='RESTRICT_VIEW_ON', text="").affect_visibility = True
        row.operator("gpencil.layer_isolate", icon='LOCKED', text="").affect_visibility = False
        layout.separator_spacer()
        row = layout.row(align=True)


def dope_handler() :
    if bpy.context.mode == 'PAINT_GPENCIL' :
        current_layers = get_active_layers()
        previous_layers = bpy.context.window_manager.dse_data.previous_active_layers
        if current_layers != previous_layers and len(previous_layers) > 0 and len(current_layers) > 0 :
            if len(current_layers) == 1 :
                bpy.context.window_manager.dse_data.new_object.clear()
                bpy.context.window_manager.dse_data.new_object.append(current_layers[0][0])
                bpy.ops.dope.switch()     
            else :
                for item in current_layers :
                    if item not in previous_layers :           
                        bpy.context.window_manager.dse_data.new_object.clear()
                        bpy.context.window_manager.dse_data.new_object.append(item[0])
                        bpy.ops.dope.switch()
                        break

        bpy.context.window_manager.dse_data.previous_active_layers.clear()
        final_layers = get_active_layers()
        for item in final_layers :
            bpy.context.window_manager.dse_data.previous_active_layers.append(item)

from .dopesheet_exposure import *
classes = [DOPE_PROPS, DOPE_OT_SWITCH_DRAWING_OBJECT, DOPE_OT_EXPO, GP_DV_AddonPref]
addon_keymaps = [] 


def register():
    for cls in classes :
        bpy.utils.register_class(cls)
    bpy.types.WindowManager.dse_data = bpy.props.PointerProperty(type = DOPE_PROPS)
    handler = bpy.types.SpaceDopeSheetEditor.draw_handler_add(dope_handler, (), 'WINDOW', 'POST_PIXEL')
    bpy.context.window_manager.dse_data.draw_handler.clear()
    bpy.context.window_manager.dse_data.draw_handler.append(handler)
    bpy.types.DOPESHEET_HT_header.append(draw)
    kc = bpy.context.window_manager.keyconfigs.addon
    if kc :
        km = kc.keymaps.new(name="Dopesheet", space_type='DOPESHEET_EDITOR')
        kmi = km.keymap_items.new("dope.expo", type = 'F5', value = 'PRESS', repeat = True)
        kmi.properties.increase = True
        kmi.active = True
        addon_keymaps.append((km, kmi))

        km = kc.keymaps.new(name="Dopesheet", space_type='DOPESHEET_EDITOR')
        kmi = km.keymap_items.new("dope.expo", type = 'F5', value = 'PRESS', shift = True, repeat = True)
        kmi.properties.increase = False
        kmi.active = True
        addon_keymaps.append((km, kmi))

def unregister():
    for handler in bpy.context.window_manager.dse_data.draw_handler :
        bpy.types.SpaceDopeSheetEditor.draw_handler_remove(handler, 'WINDOW')    
    del bpy.types.WindowManager.dse_data
    for cls in classes :
        bpy.utils.unregister_class(cls)
    # handle the keymap
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()
    bpy.types.DOPESHEET_HT_header.remove(draw)

