# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import bpy
from bpy.types import PropertyGroup

def intersection(lst1, lst2):
    lst3 = []
    for i1 in lst1:
        for i2 in lst2:
            if i1 == i2:
                lst3.append(i1)
    return lst3

def redraw(current_area):
    for region in current_area.regions:
                region.tag_redraw()

def get_all_next_kf(current_tuple):
    current_fc = current_tuple[0]
    current_kf = current_tuple[1]
    current_frame = current_kf.co[0]
    next_kf_list = []
    next_kf_list.append(current_tuple)
    for kf in current_fc.keyframe_points:
        if current_frame < kf.co[0]:
            next_kf_list.append((current_fc, kf))
    return next_kf_list

def get_all_previous_kf(current_tuple):
    current_fc = current_tuple[0]
    current_kf = current_tuple[1]
    current_frame = current_kf.co[0]
    next_kf_list = []
    for kf in current_fc.keyframe_points:
        if current_frame > kf.co[0]:
            next_kf_list.append((current_fc, kf))
    next_kf_list.append(current_tuple)
    return next_kf_list

def get_all_next_frames(current_tuple):
    current_gp = current_tuple[0]
    current_f = current_tuple[1]
    current_frame = current_f.frame_number
    next_kf_list = []
    next_kf_list.append(current_tuple)
    for f in current_gp.frames:
        if current_frame < f.frame_number:
            next_kf_list.append((current_f, f))
    return next_kf_list

def get_all_previous_frames(current_tuple):
    current_gp = current_tuple[0]
    current_f = current_tuple[1]
    current_frame = current_f.frame_number
    next_kf_list = []
    for f in current_gp.frames:
        if current_frame > f.frame_number:
            next_kf_list.append((current_f, f))
    next_kf_list.append(current_tuple)
    return next_kf_list

def increase_kf_list(list, increment:float):
    for x in range(len(list)):
        if x != 0:
            kf = list[x][1]
            kf.co[0] = kf.co[0] + increment

def decrease_kf_list(list, increment:float):
    next_kf = get_all_next_kf(list[len(list) - 1])
    for x in range(len(next_kf)):
        if x != len(next_kf):
            if next_kf[x][1].co[0] - increment > next_kf[x-1][1].co[0]:
                kf = next_kf[x][1]
                kf.co[0] = kf.co[0] - increment


def increase_frames_list(list, increment:int):
    for x in range(len(list)):
        if x != 0:
            f = list[x][1]
            f.frame_number = f.frame_number + increment

def decrease_frames_list(list, increment:float):
    next_f = get_all_next_frames(list[len(list) - 1])
    for x in range(len(next_f)):
        if x != len(next_f):
            if next_f[x][1].frame_number - increment > next_f[x-1][1].frame_number:
                f = next_f[x][1]
                f.frame_number = f.frame_number - increment
            
def add_exposure(tuple_list):
    for item in tuple_list:
        if str(type(item[0])) == "<class 'bpy.types.FCurve'>":
            current_and_next_kf = get_all_next_kf(item)
            increase_kf_list(current_and_next_kf, 1.0)
        if str(type(item[0])) == "<class 'bpy.types.GPencilLayer'>":
            current_and_next_f = get_all_next_frames(item)
            increase_frames_list(current_and_next_f, 1)

def remove_exposure(tuple_list):
    for item in tuple_list:
        if str(type(item[0])) == "<class 'bpy.types.FCurve'>": #isinstance(C.object, bpy_types.Object)
            current_and_prev_kf = get_all_previous_kf(item)
            decrease_kf_list(current_and_prev_kf, 1.0)
        if str(type(item[0])) == "<class 'bpy.types.GPencilLayer'>":
            current_and_prev_f = get_all_previous_frames(item)
            decrease_frames_list(current_and_prev_f, 1)

def filter_by_object_name(lst, name:str):
    return [ item for item in lst if name in item.name ]

def filter_by_layers_name(tuple_list, name:str):
    layers_or_fcurves = []
    for tuple in tuple_list:
        if tuple[0] is not None:
            layers_or_fcurves.append(tuple)
    return [ item for item in layers_or_fcurves if name in get_layer_or_fcurve_name(item) ]

def get_layer_or_fcurve_name(current_tuple):
    # voir https://blender.stackexchange.com/questions/250637/get-user-friendly-name-for-f-curve pour mieux comprendre ma démarche
    axis_name = [ "X", "Y", "Z" ]
    quat_name = [ "W", "X", "Y", "Z" ]
    layer_or_fcurve = current_tuple[0]
    if str(type(layer_or_fcurve)) == "<class 'bpy.types.FCurve'>":
        if layer_or_fcurve.data_path in ["location", "rotation_euler", "rotation_quaternion", "scale"]:
            if "quaternion" in layer_or_fcurve.data_path:
                if quat_name[layer_or_fcurve.array_index] == "X":
                    return "X Euler Rotation"
                if quat_name[layer_or_fcurve.array_index] == "Y":
                    return "Y Euler Rotation"
                if quat_name[layer_or_fcurve.array_index] == "Z":
                    return "Z Euler Rotation"
                if quat_name[layer_or_fcurve.array_index] == "W":
                    return "W Euler Rotation"
            else:
                if axis_name[layer_or_fcurve.array_index] == "X":
                    return "X Location"
                if axis_name[layer_or_fcurve.array_index] == "Y":
                    return "Y Location"
                if axis_name[layer_or_fcurve.array_index] == "Z":
                    return "Z Location"
    elif str(type(layer_or_fcurve)) == "<class 'bpy.types.GPencilLayer'>":
        return layer_or_fcurve.info

def get_fcurves_kfcp_from_obj(obj):
    anim_data = obj.animation_data
    if anim_data is not None and anim_data.action is not None:
        return [ (fc,k) for fc in anim_data.action.fcurves for k in fc.keyframe_points if k.select_control_point ]

def get_layers_gpframes_from_obj(obj):
    data = obj.data
    layer_and_gpframes = []
    for layer in data.layers:
        for frame in layer.frames:
            if frame.select == True:
                layer_and_gpframes.append((layer, frame))
    return layer_and_gpframes

def get_anim_data_from_list(obj_list):
    result = [] #[[fc_kf, gpfr]]
    fc_kf = []
    gpfr = []
    for item in obj_list:
        if item.type == 'GPENCIL':
            current_gpfr = get_layers_gpframes_from_obj(item)
            if len(current_gpfr) > 0:
                gpfr = gpfr + current_gpfr # get all (layer, GPencil frames)
        current_fc_kf = get_fcurves_kfcp_from_obj(item)
        if current_fc_kf is not None:
            fc_kf = fc_kf + current_fc_kf # get all (fcurve, kf control points) 
    result = result + fc_kf
    result = result + gpfr
    return result 

def get_dopesheet_visible_objects(space) :
    window = bpy.context.window_manager
    result = []

    # premier filtrage par état du dopesheet
    if space.dopesheet.show_only_selected == True:
        for obj in bpy.context.selected_objects:
            result.append(obj)
    else:
        if space.dopesheet.show_hidden == False :
            for obj in bpy.context.visible_objects : 
                result.append(obj)
        else :
            for obj in bpy.context.scene.objects :
                result.append(obj)

    # filtrer par collection
    if space.dopesheet.filter_collection is not None:
        result = intersection(result, space.dopesheet.filter_collection.all_objects)
    return result

class DOPE_OT_EXPO(bpy.types.Operator) :
    bl_label = "Change Exposure"
    bl_idname = 'dope.expo' #dope.expo
    bl_options = {'REGISTER'}  

    increase : bpy.props.BoolProperty()

    @classmethod
    def poll(cls, context):
        return context.area.type == 'DOPESHEET_EDITOR'

    def execute(self, context):
        #determiner l'area (prend la première dopesheet)
        screen = bpy.context.screen
        ds_space = context.area.spaces[0]

        if ds_space :
            selected = get_dopesheet_visible_objects(ds_space)
            if selected is not None:
                a_d = get_anim_data_from_list(selected)
                a_d = filter_by_layers_name(a_d, ds_space.dopesheet.filter_fcurve_name) # ATTENTION: sensible à la casse...
            if self.increase == True:
                add_exposure(a_d)
            else:
                remove_exposure(a_d)
            
            redraw(bpy.context.area)

        return {'FINISHED'}

# addon_keymaps = [] 
# def register():
#     bpy.utils.register_class(DOPE_OT_EXPO)
#     # Keymapping
#     kc = bpy.context.window_manager.keyconfigs.addon
#     if kc :
#         km = kc.keymaps.new(name="Dopesheet", space_type='DOPESHEET_EDITOR')
#         kmi = km.keymap_items.new("op.test_wac", type = 'F5', value = 'PRESS')
#         kmi.properties.increase = True
#         kmi.active = True
#         addon_keymaps.append((km, kmi))

#         km = kc.keymaps.new(name="Dopesheet", space_type='DOPESHEET_EDITOR')
#         kmi = km.keymap_items.new("op.test_wac", type = 'F5', value = 'PRESS', shift = True)
#         kmi.properties.increase = False
#         kmi.active = True
#         addon_keymaps.append((km, kmi))

# def unregister():
#     bpy.utils.unregister_class(DOPE_OT_EXPO)

#     # handle the keymap
#     for km, kmi in addon_keymaps:
#         km.keymap_items.remove(kmi)
#     addon_keymaps.clear()
